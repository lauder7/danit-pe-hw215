
lbMain: {
    let nNumber = inputNatural0(`Factorial calculation. Enter a natural number`);
    if (nNumber == null) break lbMain;

    // For better large number representation use console.log()
    // console.log(`Factorial ${nNumber}! = ${calcFactorial(nNumber)}`);
    alert(`Factorial ${nNumber}! = ${calcFactorial(nNumber)}`);
}



function inputNatural0(sMsg) {
    let sNum = "";
    let nNum;

    do {
        sNum = prompt(sMsg, sNum);
        if (sNum === null) return null;
        nNum = +sNum;
    } while (sNum === "" || Number.isNaN(nNum) || !Number.isInteger(nNum) || nNum < 0);
    
    return nNum;
}



function calcFactorial(nNum) {
    if (nNum === 0) return BigInt(1);
    return BigInt(nNum) * calcFactorial(nNum - 1);
}
